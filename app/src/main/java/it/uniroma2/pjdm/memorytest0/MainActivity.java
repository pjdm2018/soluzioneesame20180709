package it.uniroma2.pjdm.memorytest0;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {
    private final int CARDSNUMBER = ('z' - 'a') * 2;
    private final int SHUFFLES = 100; // how many shuffling iterations
    private Card[] cards = new Card[CARDSNUMBER];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int i = 0;
        for(char c = 'a'; c < 'z'; c++) {
            /* cycle through the resources */
            String cardName = Character.toString(c);
            int cardId = getResources().getIdentifier( cardName, "drawable", getPackageName() );
            Bitmap bmp = BitmapFactory.decodeResource( getResources(), cardId);

            /* instantiate two cards with this resource */
            cards[i]     = new Card( this, cardName, bmp);
            cards[i + 1] = new Card( this, cardName, bmp);

            i += 2;
        }

        /* shuffle the cards */
        for(i = 0; i < SHUFFLES; i++) {
            int j = (int)Math.floor(Math.random() * CARDSNUMBER);
            int k = (int)Math.floor(Math.random() * CARDSNUMBER);
            if(j != k) {
                Card tmpCard = cards[j];
                cards[j] = cards[k];
                cards[k] = tmpCard;
            }
        }

        /* associate the adapter to the GridView */
        GridView gridview = findViewById(R.id.gridview);
        gridview.setAdapter(new CardsAdapter(this, R.id.mainlayout, cards));
    }

}
