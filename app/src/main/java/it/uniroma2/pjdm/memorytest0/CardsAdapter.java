package it.uniroma2.pjdm.memorytest0;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

/**
 * Created by clauz on 7/4/18.
 */

public class CardsAdapter extends ArrayAdapter<Card> {
    private Card[] cards;

    public CardsAdapter(@NonNull Context context, int resource, @NonNull Card[] cards) {
        super(context, resource, cards);
        this.cards = cards;
    }

    private int getDiscovered() {
        /* cycle the cards array and return the first discovered card */
        for(int i = 0; i < cards.length; i++)
            if(cards[i].getState() == Card.STATE_DISCOVERED)
                return i;
        /* return -1 if no card is discovered */
        return -1;
    }

    private int getNDiscovered() {
        /* cycle the cards array and return the number of discovered cards */
        int count = 0;
        for(int i = 0; i < cards.length; i++)
            if(cards[i].getState() == Card.STATE_DISCOVERED)
                count ++;
        return count;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ImageView iv;

        if(convertView != null) {
            iv = (ImageView) convertView;
        } else {
            iv = new ImageView(super.getContext());
        }

        /* set the imageview bitmap from the card object */
        final Card card = cards[position];
        iv.setImageBitmap(card.getBmp());

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* game logic */
                /* the clicked ImageView */
                ImageView imageview = (ImageView) view;
                int n = getNDiscovered(); /* number of currently discovered cards */
                int d = getDiscovered();  /* discovered card (if any) */

                if(n == 0) {
                    /* no cards are discovered, discover the clicked card */
                    card.setState(Card.STATE_DISCOVERED);
                    imageview.setImageBitmap(card.getBmp());
                } else if(n == 1) {
                    /* one card is currently discovered*/
                    /* check if the previously discovered card is the same as the clicked one*/
                    Card prevCard = cards[d];
                    if(prevCard.getName() == card.getName() && card.getState() == Card.STATE_COVERED) {
                        /* if it's the same, the cards match, hide both cards */
                        prevCard.setState(Card.STATE_GONE);
                        card.setState(Card.STATE_GONE);
                    } else {
                        /* if it's not the same, discover the current card */
                        card.setState(Card.STATE_DISCOVERED);
                    }
                } else if(n == 2) {
                    /* two (unmatched) cards are currently discovered */
                    /* cover the previously discovered cards and discover the current card */
                    Card prevCard = cards[d];
                    prevCard.setState(Card.STATE_COVERED);
                    /* get the other discovered card */
                    d = getDiscovered();
                    prevCard = cards[d];
                    prevCard.setState(Card.STATE_COVERED);

                    /* discover the currently clicked card */
                    card.setState(Card.STATE_DISCOVERED);
                }

                /* cover the currently clicked card after 4 seconds */
                if(card.getHandler() != null) {
                    /* remove messages, if any */
                    card.getHandler().removeMessages(1);
                } else {
                    Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            card.setState(Card.STATE_COVERED);
                            notifyDataSetChanged();
                        }
                    };
                    card.setHandler(handler);
                }
                /* send a message in the future */
                Message m = card.getHandler().obtainMessage();
                m.what = 1;
                card.getHandler().sendMessageDelayed(m, 4000);

                /* update the gridview */
                notifyDataSetChanged();
            }
        });

        return iv;
    }
}
