package it.uniroma2.pjdm.memorytest0;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;

/**
 * Created by clauz on 7/4/18.
 */

public class Card {
    private Context ctx;
    private String name;
    private Bitmap discoveredBmp;
    private Bitmap coveredBmp;
    private Bitmap goneBmp;
    private int state;
    private Handler handler;

    public final static int STATE_COVERED = 0;    /* card is covered */
    public final static int STATE_DISCOVERED = 1; /* card is uncovered */
    public final static int STATE_GONE = 2;       /* card is gone/transparent */

    public Card(Context ctx, String name, Bitmap bmp) {
        this.ctx = ctx;
        this.name = name;
        this.discoveredBmp = bmp;
        /* the card is initially covered */
        this.state = STATE_COVERED;

        /* load resources for the cardback and transparency */
        this.coveredBmp = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.back);
        this.goneBmp = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.transparent);
    }

    public void setState(int state) {
        /* do not allow to change state from STATE_GONE */
        if(this.state != STATE_GONE)
            this.state = state;
    }

    public int getState() {
        return this.state;
    }

    public String getName() {
        return this.name;
    }

    public Bitmap getBmp() {
        switch(state) {
            case STATE_COVERED:
                return coveredBmp;
            case STATE_DISCOVERED:
                return discoveredBmp;
            default:
                return goneBmp;
        }
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
